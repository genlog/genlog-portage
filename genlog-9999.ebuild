# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2

DESCRIPTION="Bash script that generates logs and creates a paste2.org link, useful when posting on support forums and helps addressing common problems on a GNU/Linux machine"
HOMEPAGE="http://www.gitorious.org/genlog"
SRC_URI=""
EGIT_REPO_URI="git://gitorious.org/genlog/genlog.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~spar x86"

DEPEND="sys-apps/pciutils
	sys-apps/usbutils
	net-misc/wget
	app-text/pastebinit
	app-shells/bash"
RDEPEND="${DEPEND}"

src_install() {
	newbin genlog.sh genlog || die
}
